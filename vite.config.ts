import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    VitePWA({
      injectRegister: 'inline',
      registerType: 'prompt',

      includeAssets: [
        'favicon.ico',
        'apple-touch-icon.png',
        '/icon-192-maskable.png',
      ],

      manifest: {
        name: 'pwa-vite',
        description: 'An app to maintain your daily objectives',
        theme_color: '#6a3de8',

        icons: [
          {
            src: 'icon-512.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'icon-192.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
      },

      devOptions: {
        enabled: true,
      },
    }),
  ],
});

